import struct
import math
import mouse
import pyautogui
import keyboard
from screeninfo import get_monitors


from pymouse import PyMouse
from pykeyboard import PyKeyboard

m = PyMouse()

import subprocess

show_output = False

paused = False

print(subprocess.check_output(["xinput", "--list"]).decode("utf-8"))
id = int(input("choose an id from the above list: "))
subprocess.run(["xinput", "--disable", str(id)])

screen_width, screen_height = [get_monitors()[0].width, get_monitors()[0].height]

aim_angle = 0

rotation_sensitivity = 0.0005
y_axis_controls_length = False
aim_length_scale = 1
length_sensitivity = 0.0009

radius_percentage = 1/2  * 1/2
aim_length_x = screen_width * radius_percentage
aim_length_y = screen_height * radius_percentage
origin = (screen_width / 2, screen_height / 2)

max_distance = math.sqrt(aim_length_x ** 2 + aim_length_y ** 2)

def clamp(input, min, max):
    if (input < min):
        return min
    elif (input > max):
        return max
    return input

def translate(origin, point):
    return tuple(map(sum,zip(origin, point)))

def polar_mouse_movement(delta_x, delta_y):
    global aim_length_scale
    global aim_angle

    if y_axis_controls_length:
        aim_length_scale = clamp(aim_length_scale + delta_y * length_sensitivity, 0, 1)

    aim_angle += delta_x * rotation_sensitivity

    x_pos = aim_length_scale * aim_length_x * math.cos(aim_angle)
    y_pos = aim_length_scale * aim_length_y * math.sin(aim_angle)

    # return clamp(x_pos + origin[0], -screen_width/2, screen_width/2), clamp(y_pos + origin[1], -screen_height/2, screen_height/2)
    return x_pos + origin[0], y_pos + origin[1]

def update_mouse_position(x_pos, y_pos):
    mouse.move(x_pos, y_pos)

file = open( "/dev/input/mice", "rb" )

def getMouseEvent():
    buf = file.read(3)
    button = buf[0]
    left_pressed = button & 0x1
    middle_pressed = (button & 0x4) > 0
    right_pressed = (button & 0x2) > 0
    delta_x, delta_y = struct.unpack("bb", buf[1:])
    if show_output:
        print ("L:%d, M: %d, R: %d, delta_x: %d, delta_x: %d\n" % (left_pressed, middle_pressed, right_pressed, delta_x, delta_y))
    return left_pressed, middle_pressed, right_pressed, delta_x, delta_y


def toggle_mouse():
    global paused
    # if paused unpause, and vise versa
    if paused:
        subprocess.run(["xinput", "--disable", str(id)]) # disabling means the polar will be activated
    else:
        subprocess.run(["xinput", "--enable", str(id)]) # enabling means cartesian mouse will be activated
        mouse.move(origin[0], origin[1])
    paused = not paused

def end_program():
    subprocess.run(["xinput", "--enable", str(id)])
    file.close()
    exit()

def enable_length_control():
    global y_axis_controls_length
    y_axis_controls_length = not y_axis_controls_length


keyboard.add_hotkey('ctrl+alt+shift+t', toggle_mouse)
keyboard.add_hotkey('ctrl+alt+shift+q', end_program)
keyboard.add_hotkey('ctrl+alt+shift+c', enable_length_control)


# import time
# oldtime = time.time()
# # check
# 
# times_looped = 0
while True:

    if not paused:
        left_pressed, middle_pressed, right_pressed, delta_x, delta_y = getMouseEvent()
        x_pos, y_pos = polar_mouse_movement(delta_x, delta_y)

        if left_pressed:
            # mouse.click()
            m.click(int(x_pos), int(y_pos))

        update_mouse_position(x_pos, y_pos)

    # times_looped += 1

    # if time.time() - oldtime > 1:
    #     oldtime = time.time()
    #     print(f"iterated: {times_looped} in a second");
    #     times_looped = 0



